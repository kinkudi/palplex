from base64 import b64encode,b64decode
from json import JSONDecoder
from urllib import urlencode
from google.appengine.api import urlfetch
from google.appengine.api import xmpp
from google.appengine.ext.webapp import RequestHandler
from google.appengine.ext.webapp import WSGIApplication 
from google.appengine.ext.webapp.util import run_wsgi_app 

UTF8 = 'utf-8'
ALPHA = '-_'.encode(UTF8)
PALOPEN = "pal says, \""
PALCLOSE = "\""
CHAT = xmpp.MESSAGE_TYPE_CHAT
paired = dict()
lonelies = list()
pending = dict()
fromJson = JSONDecoder()
#Google Talk Gadget for Hosted Domain (Apps) Mobile
APPS_MTALK_HEAD = 'https://hostedtalkgadget.google.com/a/'
#insert user domain
APPS_MTALK_TAIL = '/talkgadget/m'
#Google Talk Gadget for Mobile
MTALK = 'https://talkgadget.google.com/talkgadget/m'
#get, +state (users email)
STATE = 'state'
AUTH = 'https://accounts.google.com/o/oauth2/auth?' + urlencode({
 'response_type': 'code',
 'client_id': '180876035307.apps.googleusercontent.com',
 'redirect_uri': 'https://palplex.appspot.com/spreadYourPlex',
 'scope': 'https://www.google.com/m8/feeds',
 'access_type': 'online' #could be offline
}) + '&state='
#post, +code
ACCS = 'https://accounts.google.com/o/oauth2/token'
ACCS_POSTDATA = urlencode({
 'client_id' : '180876035307.apps.googleusercontent.com',
 'client_secret' : 'MXAXRT4YJM0exedB44Q4h6uP',
 'redirect_uri' : 'https://palplex.appspot.com/spreadYourPlex',
 'grant_type' : 'authorization_code'
})
ACCS_HEADERS = { 'Content-Type' : 'application/x-www-form-urlencoded' }
CODE = 'code'
#template of success resp
TKRP = {
  'access_token' : '',
  'refresh_token' : '',
  'expires_in' : '',
  'token_type' : ''
}
#post, use a refresh token, + refresh_token
RFRS = 'https://accounts.google.com/o/oauth2/token' 
RFRS_POSTDATA = {
 'client_id' : '180876035307.apps.googleusercontent.com',
 'client_secret' : 'MXAXRT4YJM0exedB44Q4h6uP',
 'grant_type' : 'refresh_token'
}
#get + access_token
REQT = 'https://www.google.com/m8/feeds/contacts/default/full?' + urlencode({
  'orderby' : 'lastmodified',
  'sortorder' : 'descending'
})
ACCESS_TOKEN = 'access_token'

FORM_EMAIL_ID = 'emailAddress'

ERROR = 'error'
ACCESS_DENIED = 'access_denied'


#in REQT it would be good to identify those contacts having
#only been in IM, as IM contacts. that would be cool.
#and only post to those in Google Talk.


#the 16 commands
INVITE = 'goplex'
GONEXT = 'gonex'
GOFLAG = 'goflag'
PLEXNOW = 'plexnow'
PLEXME = 'plexme'
WEPLEX = 'weplex'
SPYOK = 'spyok'
OKSPY = 'okspy'
TELLME = 'tellme'
TELLALL = 'tellall'
NOTELL = 'notell'
NEVERTELL = 'nevertell'
LIKE = 'like'
LOL = 'lol'
STATS = 'showplex'
NOPLEX = 'noplex'

EMAIL_TAG = 'gd:email'
EMAIL_ADDRESS_KEY = 'address'
ADDRESS_DELIM = "'"
NATIVE_USER = 'gmail.com'
NATIVE_UK_USER = 'googlemail.com'
PALPAL = "palplex.appspotchat.com"
PALNATIVE = "palplex@appspot.com"
BUCKET_SERVICE = "palplex2@appspot.com"
ENDPOINT = "cris@stringfellow"

#response will terminate with 'next' link if more to serve
#we can always send invite to google (and if we can spec them, apps, too) and send emails to other contacts inviting them to make an account on goodle so they can use this

#if bucket service goes down
#this is still self contained random chat
        
class Scribe(RequestHandler):
    def post(self):
        message = xmpp.Message(self.request.POST)
        user = message.sender.split('/',1)[0]
        touser,todomain = message.to.split('@',1)
        if todomain.startswith(PALPAL): #any message on this channel that's not a bucket we invite
            if touser == "SPORT":
                touser = touser #rout message through the sport bucket
            else:
                xmpp.send_message(user, ' '.join([''.join([AUTH,b64encode('/'.join([user,message.to]).encode(UTF8),ALPHA)]),"\nHey!",touser,"wants you to join them on Palplex, a new social utility that lets you grow your social network with people you'll like right here in Google Chat. \nLogin above to share Palplex with some of your pals and we'll send you an invite to the main Palplex bucket. Palplex protects your privacy and anonymity and gives you full control over who you chat to and what you share. Your email address is never revealed to your Palplex pals unless you do so yourself. Grow your network with new people you'll like. \n"]), message.to, CHAT)
            return
        command = message.body.split(' ',1)[0]
        if command == INVITE: # or if first message. also, make HTML 
            xmpp.send_message(user, ''.join([AUTH,b64encode(user.split('@',1).encode(UTF8),ALPHA)]), None, CHAT)
        elif user in paired:   #change pairing for golive
            xmpp.send_message(paired[user], ''.join([PALOPEN, message.body, PALCLOSE]), None, CHAT)
        elif len(lonelies) != 0:
            userpair = lonelies.pop()
            if userpair != user:
                paired[user] = userpair
                paired[userpair] = user
            else:
                lonelies.append(userpair)
        else:
             lonelies.append(user)
        
class FinalHurdle(RequestHandler):
    def get(self):
        codestate = self.request.params
        if ERROR in codestate:
            if codestate[ERROR] == ACCESS_DENIED:
                self.response.out.write("<body><p><a href='palplex.appspot.com'>Click here</a> to go back to Palplex.</p><p> You can always invite your pals later. But if you want to get an invite, please invite at least 2 other Google Account users who are interested in Palplex. You can still add Palplex to your contacts but you won't be able to chat with others until you add at least 2.</p><p> The plexverse needs to grow. Only you can help it.</p></body>")
            else:
                self.response.out.write("Oops...There was an error and we could not authenticate. That's all we know.")
        else:
            username = unicode(b64decode(codestate[STATE].encode(UTF8),ALPHA), UTF8)
            result = urlfetch.fetch(url=ACCS,
                                    payload='&'.join([ACCS_POSTDATA,urlencode({CODE:codestate[CODE]})]),
                                    method=urlfetch.POST,
                                    headers=ACCS_HEADERS)
            if result.status_code == 200:
                if '@' in username:
                    username, fromuser = username.split('/',1)
                    xmpp.send_message(username, "Hey! We've just sent you an invite to the main Palplex bucket. Check your chat.", fromuser, CHAT)
                    xmpp.send_invite(username, PALNATIVE)
                    username = username.split('@',1)[0]
                self.response.out.write("Authent is successful! We got your access token.<br/>") # replace for golive
                params = fromJson.decode(result.content)
                contacts = urlfetch.fetch(url='&'.join([REQT,urlencode({ACCESS_TOKEN:params[ACCESS_TOKEN]})]),
                                          method=urlfetch.GET)
                if contacts.status_code == 200:
                    content = contacts.content
                    start = content.find(EMAIL_TAG,0)
                    jids = []
                    while(start != -1):
                        start = content.find(EMAIL_ADDRESS_KEY,start)
                        address_start = content.find(ADDRESS_DELIM,start)+1
                        address_end = content.find(ADDRESS_DELIM,address_start)
                        jids.append(content[address_start:address_end])
                        start = content.find(EMAIL_TAG,address_end)
                    self.response.out.write('<br/>'.join(jids))#remove when golive
                    for jid in jids:
                       if jid == 'crisfolio@gmail.com': #remove when golive:
                           xmpp.send_invite(jid,'@'.join([username,PALPAL])) #make another invite
                else:
                    self.response.out.write("Something dastardly happened."+contacts.content)

            else:
                self.response.out.write("Something horrible happened. "+result.content)

class Hooked(RequestHandler):
    def post(self):
        user = self.request.get('from').split('/',1)[0]
        messageto = self.request.get('to').split('/',1)[0]
        touser,todomain = messageto.split('@',1)
        if todomain.startswith(PALPAL):
            if touser == 'SPORT':
                touser = touser #buckets
            else:
                xmpp.send_message(user, ' '.join([''.join([AUTH,b64encode('/'.join([user,messageto]).encode(UTF8),ALPHA)]),"\nHey!",touser,"wants you to join them on Palplex, a new social utility that lets you grow your social network with people you'll like right here in Google Chat. \nLogin above to share Palplex with some of your pals and we'll send you an invite to the main Palplex bucket. Palplex protects your privacy and anonymity and gives you full control over who you chat to and what you share. Your email address is never revealed to your Palplex pals unless you do so yourself. Grow your network with new people you'll like. \n"]), messageto, CHAT)
            return
        else:
            xmpp.send_message(user,"Hey, welcome to Palplex. You're in the main bucket.", None, CHAT)

                             
class FirstHurdle(RequestHandler):
    def post(self):
        user = self.request.POST[FORM_EMAIL_ID]
        username = user.split('@',1)[0]
        xmpp.send_invite(user,'@'.join([username,PALPAL]))
        self.response.out.write(''.join(["<body>You can contribute to the Plexverse by inviting all your contacts to Plex.<br/><a href='",AUTH,b64encode(username.encode(UTF8),ALPHA),"'>Help make Plex the best way to connect with people you'll like by letting your contacts get Plex too.</a> Click to get your palplex invite. </body>"]))

    def get(self):
        user = self.request.params[FORM_EMAIL_ID]
        username = user.split('@',1)[0]
        xmpp.send_invite(user,'@'.join([username,PALPAL]))
        self.response.out.write(''.join(["<body>You can contribute to the Plexverse by inviting all your contacts to Plex.<br/><a href='",AUTH,b64encode(username.encode(UTF8),ALPHA),"'>Help make Plex the best way to connect with people you'll like by letting your contacts get Plex too.</a> Click to get your palplex invite. </body>"]))

application = WSGIApplication([
    ('/_ah/xmpp/message/chat/', Scribe),
    ('/spreadYourPlex', FinalHurdle),
    ('/inviteMyPlex', FirstHurdle),
    ('/_ah/xmpp/subscription/subscribe/', Hooked),
    ])

def main():
  run_wsgi_app(application)

if __name__ == '__main__':
  main()

